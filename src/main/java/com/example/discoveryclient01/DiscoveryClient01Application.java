package com.example.discoveryclient01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@EnableEurekaClient
public class DiscoveryClient01Application {

    public static void main(String[] args) {
        SpringApplication.run(DiscoveryClient01Application.class, args);
    }





}
